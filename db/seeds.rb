# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# Load fixtures!
#

LapPoint.destroy_all

require 'csv'

Dir.glob(Rails.root.join('db/fixtures/*/*.csv')).each do |filepath|
  next if filepath =~ /real_data/

  parts = filepath.split('/')

  session_name = parts.last(2).join('_').gsub('.csv', '')

  header = nil

  CSV.read(filepath).each do  |row|
    unless header
      header = {
        lap_number: row.index('Lap #'),
        recorded_at: row.index('Time (s)'),
        x_position: row.index('X-position (m)'),
        y_position: row.index('Y-position (m)'),
        distance: row.index('Distance (m)'),
        speed: row.index('Speed (m/s)'),
        altitude: row.index('Altitude (m)'),
        bearing: row.index('Bearing (deg)'),
        lat_acc: row.index('Lateral acceleration (m/s2)'),
        long_acc: row.index('Longitudinal acceleration (m/s2)'),
        device_update_rate: row.index('Device update rate (Hz)'),
        vertical_speed: row.index('Vertical speed (m/s)'),
        elapsed_time: row.index('Elapsed time (s)'),
        lean_angle: row.index('Lean angle (deg)'),
        combined_acc: row.index('Combined acceleration (m/s2)'),
        latitude: row.index('Latitude (deg)'),
        longitude: row.index('Longitude (deg)'),
        satelites: row.index('Satellites (sats)'),
        fix_type: row.index('Fix type'),
        precision_dop: row.index('Coordinate precision (DOP)'),
        altitude_precision_dop: row.index('Altitude precision (DOP)')
      }
      next
    end

    value_at = ->(key) { row[header[key]] if header[key] }


    lap_number = value_at.call(:lap_number).to_i

    next unless lap_number > 0

    data = {
      session: session_name,
      lap_number: lap_number,
      recorded_at: value_at.call(:recorded_at).to_f,
      x_position: value_at.call(:x_position).to_f,
      y_position: value_at.call(:y_position).to_f,
      distance: value_at.call(:distance).to_f,
      speed: value_at.call(:speed).to_f,
      altitude: value_at.call(:altitude).to_f,
      bearing: value_at.call(:bearing).to_f,
      lat_acc: value_at.call(:lat_acc).to_f,
      long_acc: value_at.call(:long_acc).to_f,
      device_update_rate: value_at.call(:device_update_rate).to_f,
      vertical_speed: value_at.call(:vertical_speed).to_f,
      elapsed_time: value_at.call(:elapsed_time).to_f,
      lean_angle: value_at.call(:lean_angle).to_f,
      combined_acc: value_at.call(:combined_acc).to_f,
      latitude: value_at.call(:latitude).to_f,
      longitude: value_at.call(:longitude).to_f,
      satelites: value_at.call(:satelites).to_i,
      fix_type: value_at.call(:fix_type).to_f,
      precision_dop: value_at.call(:precision_dop).to_f,
      altitude_precision_dop: value_at.call(:altitude_precision_dop).to_f,
    }

    # puts row

    LapPoint.create!(data)

    # puts "Created!"
    # puts LapPoint.last.inspect
    # break
  end

  puts "#{session_name} created! #{LapPoint.where(session: session_name).count} points in total"
end
