# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_26_113341) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "lap_points", force: :cascade do |t|
    t.float "recorded_at", null: false
    t.string "session", null: false
    t.integer "lap_number", null: false
    t.float "x_position"
    t.float "y_position"
    t.float "distance"
    t.float "speed"
    t.float "altitude"
    t.float "bearing"
    t.float "lat_acc"
    t.float "long_acc"
    t.float "device_update_rate"
    t.float "vertical_speed"
    t.float "elapsed_time"
    t.float "lean_angle"
    t.float "combined_acc"
    t.float "latitude"
    t.float "longitude"
    t.float "satelites"
    t.float "fix_type"
    t.float "precision_dop"
    t.float "altitude_precision_dop"
  end

end
