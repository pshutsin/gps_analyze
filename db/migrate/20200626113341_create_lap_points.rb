class CreateLapPoints < ActiveRecord::Migration[6.0]
  def change
    create_table :lap_points do |t|
      t.float :recorded_at, null: false
      t.string :session, null: false
      t.integer :lap_number, null: false
      t.float :x_position
      t.float :y_position
      t.float :distance
      t.float :speed
      t.float :altitude
      t.float :bearing
      t.float :lat_acc
      t.float :long_acc
      t.float :device_update_rate
      t.float :vertical_speed
      t.float :elapsed_time
      t.float :lean_angle
      t.float :combined_acc
      t.float :latitude
      t.float :longitude
      t.float :satelites
      t.float :fix_type
      t.float :precision_dop
      t.float :altitude_precision_dop
    end
  end
end
