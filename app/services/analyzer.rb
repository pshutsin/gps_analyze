require 'terminal-table'
require 'geokit'

class Analyzer
  def speed_stats
    matrix = square_matrix(:speed_diff)

    puts "Matrix calculated! for #{gps_keys.inspect}"
    puts "Total point rows #{points_sequence.size}"
    puts "Rows: #{matrix.size}"
    puts "Cols: #{matrix.first.size}"
    puts "Points: #{matrix.first.first.size}"

    counts = matrix.map { |v| v.map { |v2| v2.compact.size } }.flatten.uniq

    puts "Unique points size: #{counts}"

    puts Terminal::Table.new(title: "Average speed diff", rows: square_matrix_terminal_rows(matrix, :term_average))
    puts Terminal::Table.new(title: "Speed diff SD", rows: square_matrix_terminal_rows(matrix, :term_standard_dev))
    puts Terminal::Table.new(title: "Max speed diff", rows: square_matrix_terminal_rows(matrix, :term_max))

    nil
  end

  def distance_stats
    matrix = square_matrix(:distance_diff)

    puts "Matrix calculated! for #{gps_keys.inspect}"
    puts "Total point rows #{points_sequence.size}"
    puts "Rows: #{matrix.size}"
    puts "Cols: #{matrix.first.size}"
    puts "Points: #{matrix.first.first.size}"

    counts = matrix.map { |v| v.map { |v2| v2.compact.size } }.flatten.uniq

    puts "Unique points size: #{counts}"

    puts Terminal::Table.new(title: "Average distance diff", rows: square_matrix_terminal_rows(matrix, :term_average))
    puts Terminal::Table.new(title: "Distance diff SD", rows: square_matrix_terminal_rows(matrix, :term_standard_dev))
    puts Terminal::Table.new(title: "Max distance diff", rows: square_matrix_terminal_rows(matrix, :term_max))

    nil
  end

  def record_at(float, source)
    source.each do |v|
      return v if (v.recorded_at - float).abs <= 0.05

      return nil if v.recorded_at > float + 1
    end

    return nil
  end

  def gps_keys
    @gps_keys ||= %i[vbox xgps160 xgps150 qstarz]
  end

  private

  def points_sequence
    @points_sequence ||= begin
      record_arrays = gps_keys.map { |key| LapPoint.public_send(key).ordered.to_a }

      result = []

      record_arrays.first.each do |gps_point|
        row = [gps_point]
        record_arrays[1..-1].each do |r_arr|
          row << record_at(gps_point.recorded_at, r_arr)
        end

        result << row
      end

      result # array of rows, each row is a row of 4 points at the same time point.
    end
  end

  def speed_diff(point_a, point_b)
    return if !point_a || !point_b
    return 0 if point_a == point_b

    ((point_a.speed - point_b.speed) * 3600 / 1000).abs
  end

  def distance_diff(point_a, point_b)
    return if !point_a || !point_b
    return 0 if point_a == point_b

    Geokit::LatLng.new(point_a.latitude, point_a.longitude)
      .distance_to(Geokit::LatLng.new(point_b.latitude, point_b.longitude), units: :kms)*1000
  end


  def square_matrix(method_name)
    result = []
    gps_keys.size.times do
      result << []
    end

    points_sequence.each do |points_row|
      points_row.each.with_index do |point, i|
        points_row.each.with_index do |point2, j|
          next if j < i

          result[i][j] ||= []
          result[j][i] ||= result[i][j]

          value = send(method_name, point, point2)

          result[i][j] << value
          result[j][i] << value
        end
      end
    end

    result
  end

  def square_matrix_terminal_rows(matrix, method_name)
    asd_rows = []
    asd_rows << [''] + gps_keys
    matrix.each.with_index do |row, i|
      asd_row = [gps_keys[i]]

      gps_keys.each.with_index do |_key, j|
        asd_row << send(method_name, row[j])
      end

      asd_rows << asd_row
    end

    asd_rows
  end

  def term_average(row)
    (row.compact.sum.to_f / row.compact.size).round(3)
  end

  def term_standard_dev(row)
    avg = term_average(row)

    (row.compact.map { |i| (i - avg).abs }.sum / row.compact.size).round(3)

    #Math.sqrt(row.compact.map { |i| (i - avg)**2 }.sum / (row.compact.size - 1)).round(2)
  end

  def term_max(row)
    row.compact.max.to_f.round(3)
  end
end
