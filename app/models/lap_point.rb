class LapPoint < ApplicationRecord

  scope :qstarz, -> { where(session: '20200625_qstarz') }
  scope :xgps150, -> { where(session: '20200625_xgps150') }
  scope :xgps160, -> { where(session: '20200625_xgps160') }
  scope :vbox, -> { where(session: '20200625_vbox') }

  scope :ordered, -> { order(:lap_number, :recorded_at) }
end
